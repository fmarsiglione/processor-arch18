library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity mux2 is
	port (d0, d1: in STD_LOGIC_VECTOR(63 downto 0);
		  s: in STD_LOGIC;
	   	  y: out STD_LOGIC_VECTOR(63 downto 0));
end;

architecture mux2_arch of mux2 is
begin
	y <= d0 when s = '0' else d1;
end;
