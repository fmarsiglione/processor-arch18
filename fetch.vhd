library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity fetch is
    port (PCSrc_F, clk, reset: in STD_LOGIC;
          PCBranch_F: in STD_LOGIC_VECTOR(63 downto 0);
          imem_addr_F: out STD_LOGIC_VECTOR(63 downto 0));
end;

architecture fetch_arch of fetch is
    component mux2 is
        port (d0, d1: in STD_LOGIC_VECTOR(63 downto 0);
    		  s: in STD_LOGIC;
    		  y: out STD_LOGIC_VECTOR(63 downto 0));
    end component;

    component flopr is
        generic (N: INTEGER := 64);
    	port (clk, reset: in STD_LOGIC;
    		  d: in STD_LOGIC_VECTOR(N-1 downto 0);
    		  q: out STD_LOGIC_VECTOR(N-1 downto 0));
    end component;

    component alu is
        port (a, b: in STD_LOGIC_VECTOR(63 downto 0);
              ALUcontrol: in STD_LOGIC_VECTOR(3 downto 0);
              result: out STD_LOGIC_VECTOR(63 downto 0);
              zero: out STD_LOGIC);
    end component;

    signal PCnext, PCplus4, lastPC, b: STD_LOGIC_VECTOR(63 downto 0);
    signal ALUcontrol: STD_LOGIC_VECTOR(3 downto 0);
begin
    b <= (2 => '1', others => '0');
    ALUcontrol <= "0010";

    mux2_inst: mux2 port map (d0 => PCplus4, d1 => PCBranch_F, s => PCSrc_F, y => PCnext);
    flopr_inst: flopr port map (clk => clk, reset => reset, d => PCnext, q => lastPC);
    adder: alu port map (a => lastPC, b => b, ALUcontrol => ALUcontrol, result => PCplus4);
    imem_addr_F <= lastPC;
end architecture;
