library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity execute is
    port (aluSrc: in STD_LOGIC;
          aluControl: in STD_LOGIC_VECTOR(3 downto 0);
          PC_E, SignImm_E, readData1_E, readData2_E: in STD_LOGIC_VECTOR(63 downto 0);
          PCBranch_E, aluResult_E, writeData_E: out STD_LOGIC_VECTOR(63 downto 0);
          zero_E: out STD_LOGIC);
end;

architecture execute_arch of execute is
    component mux2 is
        port (d0, d1: in STD_LOGIC_VECTOR(63 downto 0);
              s: in STD_LOGIC;
              y: out STD_LOGIC_VECTOR(63 downto 0));
    end component;

    component alu is
        port (a, b: in STD_LOGIC_VECTOR(63 downto 0);
              ALUcontrol: in STD_LOGIC_VECTOR(3 downto 0);
              result: out STD_LOGIC_VECTOR(63 downto 0);
              zero: out STD_LOGIC);
    end component;

    component sl2 is
        generic (width: integer := 64);
        port (a : in std_logic_vector (width-1 downto 0);
              y : out std_logic_vector (width-1 downto 0));
    end component;

    signal SrcB_alu, SrcB_add: STD_LOGIC_VECTOR(63 downto 0);
    signal add_control: STD_LOGIC_VECTOR(3 downto 0);
begin
    add_control <= "0010";
    writeData_E <= readData2_E;

    sl2_inst: sl2 port map (a => SignImm_E, y => SrcB_add);
    mux2_inst: mux2 port map (d0 => readData2_E, d1 => SignImm_E, s => aluSrc, y => SrcB_alu);
    alu_inst: alu port map (a => readData1_E, b => SrcB_alu, ALUcontrol => aluControl, result => aluResult_E, zero => zero_E);
    adder: alu port map (a => PC_E, b => SrcB_add, ALUcontrol => add_control, result => PCBranch_E);
end architecture;
