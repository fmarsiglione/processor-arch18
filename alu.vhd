library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_SIGNED.all;

entity alu is
	port (a, b: in STD_LOGIC_VECTOR(63 downto 0);
			ALUcontrol: in STD_LOGIC_VECTOR(3 downto 0);
			result: out STD_LOGIC_VECTOR(63 downto 0);
            zero: out STD_LOGIC);
end;

architecture alu_arch of alu is
    constant z: STD_LOGIC_VECTOR(63 downto 0) := (others => '0');
    signal res: STD_LOGIC_VECTOR(63 downto 0);
begin
    with ALUcontrol select
	res <= a and b when "0000",
              a or b when "0001",
              a + b when "0010",
              a - b	when "0110",
              b when "0111",
			  a nor b when "1100",
              (others => 'X') when others;
    zero <= '1' when res = z else '0';
    result <= res;
end;
