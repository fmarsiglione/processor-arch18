library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity imem is
	generic (N: INTEGER := 32);
	port (addr: in STD_LOGIC_VECTOR(5 downto 0);
			q: out STD_LOGIC_VECTOR(N-1 downto 0));
end;

architecture imem_arch of imem is
	subtype word_t is STD_LOGIC_VECTOR(N-1 downto 0);
	type memory_t is array(0 to 63) of word_t;

	signal rom: memory_t := (x"f8000000", x"f8008001", x"f8010002",
									 x"f8018003", x"f8020004", x"f8028005",
									 x"f8030006", x"f8400007", x"f8408008",
									 x"f8410009", x"f841800a", x"f842000b",
									 x"f842800c", x"f843000d", x"cb0e01ce",
									 x"b400004e", x"cb01000f", x"8b01000f",
									 x"f803800f", others => x"00000000");
begin
	q <= rom(to_integer(unsigned(addr)));
end;
