library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity regfile is
	port (we3, clk: in STD_LOGIC;
			ra1, ra2, wa3: in STD_LOGIC_VECTOR(4 downto 0);
			wd3: in STD_LOGIC_VECTOR(63 downto 0);
			rd1, rd2: out STD_LOGIC_VECTOR(63 downto 0));
end;

architecture regfile_arch of regfile is
	subtype word_t is STD_LOGIC_VECTOR(63 downto 0);
	type memory_t is array(0 to 31) of word_t;

	function init_reg
		return memory_t is
		variable tmp : memory_t := (others => (others => '0'));
	begin
		for addr_pos in 0 to 30 loop
			tmp(addr_pos) := std_logic_vector(to_unsigned(addr_pos, 64));
		end loop;
		return tmp;
	end init_reg;

	signal reg_mem: memory_t := init_reg;

begin
	process(ra1, ra2, clk, reg_mem, we3)
	begin
		if rising_edge(clk) and we3 = '1' and wa3 /= "11111" then
			reg_mem(to_integer(unsigned(wa3))) <= wd3;
		end if;
        rd1 <= reg_mem(to_integer(unsigned(ra1)));
        rd2 <= reg_mem(to_integer(unsigned(ra2)));
	end process;
end;
