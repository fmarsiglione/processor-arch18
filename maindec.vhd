library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity maindec is
	port (Op: in STD_LOGIC_VECTOR(10 downto 0);
			Reg2Loc, ALUsrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch : out std_logic;
			AluOp: out STD_LOGIC_VECTOR(1 downto 0));
end;

architecture maindec_arch of maindec is
	signal isR, isCB, isSTUR, isLDUR: STD_LOGIC;
begin
	isR <= '1' when Op(7) = '0' else '0';
	isCB <= '1' when isR = '0' and Op(6) = '0' else '0';
	isSTUR <= '1' when isR = '0' and isCB = '0' and Op(1) = '0' else '0';
	isLDUR <= '1' when isR = '0' and isCB = '0' and isSTUR = '0' else '0';

	Reg2Loc <= '1' when isSTUR = '1' or isCB = '1' else
				  '0'	when isR = '1' else 'X';
	Alusrc <= '1' when isSTUR = '1' or isLDUR = '1' else '0';
	MemtoReg <= '1' when isLDUR = '1' else
					'0' when isR = '1' else 'X';
	RegWrite <= '1' when isR = '1' or isLDUR = '1' else '0';
	memRead <= '1' when isLDUR = '1' else '0';
	memWrite <= '1' when isSTUR = '1' else '0';
	Branch <= '1' when isCB = '1' else '0';
	AluOp(0) <= '1' when isCB = '1' else '0';
	AluOp(1) <= '1' when isR = '1' else '0';
end;