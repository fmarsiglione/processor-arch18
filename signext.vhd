library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity signext is
	port (a: in STD_LOGIC_VECTOR(31 downto 0);
			y: out STD_LOGIC_VECTOR(63 downto 0));
end;

architecture signext_arch of signext is
begin
	process(a)
	begin
		--bit 28 decides if instruction is type R
		if a(28) = '0' then
			y(5 downto 0) <= a(15 downto 10);
			y(63 downto 6) <= (others => '0');
		--with a(28) = '1', bit 27 decides if it's type D
		elsif a(27) = '1' then
			y(8 downto 0) <= a(20 downto 12);
			y(63 downto 9) <= (others => a(20));
		--must be type CB
		else
			y(18 downto 0) <= a(23 downto 5);
			y(63 downto 19) <= (others => a(23));
		end if;
	end process;
end;